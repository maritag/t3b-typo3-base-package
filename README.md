# TYPO3 base distribution

## Create new project

To create a new project run the following command:

	$ composer create-project --repository '{"type": "git", "url": "git@bitbucket.org:maritag/typo3-base-distribution.git"}' pluswerk/typo3-base-distribution temp-project

Answer the question

	Do you want to remove the existing VCS (.git, .svn..) history? [Y,n]?

with Y (yes) to remove all existing git history.

After that use typo3-console to setup TYPO3 on the dev environment (vagrant or docker):

	$ ./vendor/bin/typo3cms install:setup

Setup extensions:

	$ ./vendor/bin/typo3cms install:extensionsetupifpossible

## Installed packages

* [helhum/typo3-console](https://github.com/TYPO3-Console/TYPO3-Console)
* [roave/security-advisories](https://github.com/Roave/SecurityAdvisories)
* [helhum/typo3-secure-web](https://github.com/helhum/typo3-secure-web)
* [typo3-ter/realurl](https://github.com/dmitryd/typo3-realurl)